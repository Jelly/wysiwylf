function AudioLayer(layerName, manager)
{
	this.parent = manager;
	this.context = manager.context;
	
	this.source = this.context.createBufferSource();
	
	this.gainNode = this.context.createGain();
	this.gainNode.connect(this.context.destination);
	
	this.name = layerName;
	
	this.isLoaded = false;
	this.setVolume(0);
}

AudioLayer.prototype.update = function()
{
	
}

AudioLayer.prototype.play = function(syncTime)
{
	this.source.start(syncTime);
}

AudioLayer.prototype.getVolume = function()
{
	return this.gainNode.gain.value;
}

AudioLayer.prototype.setVolume = function(volume)
{
	this.gainNode.gain.value = Math.max(0, Math.min(1, volume));
}

AudioLayer.prototype.onBufferLoaded = function(buffer)
{
	this.isLoaded = true;
	
	// connects the node.
	this.source.buffer = buffer
	this.source.connect(this.gainNode);
	this.source.loop = true;
	
	this.parent.onLayerLoaded(this);
}

//////////////////////////////////////////////////////////////

function AudioManager()
{
	this.layers = new Array();
	
	this.context = this.tryGetContext();
	
	this.syncPlayTime = this.context.currentTime + 1;
	
	if (game.settingsMgr.disableMusic)
		return;
	
	this.addLayer('neutral');
	this.addLayer('islamic');
	this.addLayer('bhuddist');
	this.addLayer('scientist');
	this.addLayer('wallstreet');
	this.addLayer('voodoo');
}

AudioManager.prototype.onLayerLoaded = function(layer)
{
	console.log("Loaded music layer: " + layer.name);
	
	// Starts playing if all layers are loaded.
	for(var i in this.layers)
	{
		var l = this.layers[i];
		
		if (!l.isLoaded)
			return;
	}
	
	// Let's go!
	console.log("All music layers loaded, let's play!");
	var syncPlayTime = this.context.currentTime + 1;
	for(var i in this.layers)
	{
		this.layers[i].play(syncPlayTime);
	}
}

AudioManager.prototype.tryGetContext = function()
{
	if (this.context == null)
	{
		try {
			// Fix up for prefixing
			window.AudioContext = window.AudioContext || window.webkitAudioContext;
			this.context = new AudioContext();
		}
		catch(e) {
			alert('Web Audio API is not supported in this browser.');
		}
	}
	
	return this.context;
}

AudioManager.prototype.update = function(_game)
{
	for(var i in this.layers)
		this.layers[i].update();
		
	// gets the polar coordinates of the player in the world.
	var pos = _game.playerMgr.getPositionPolar();
	var r = pos[0];
	var t = pos[1];
	//console.log(r + " r/t " + t);
	
	//// sound adjustments
	
	var pi4 = Math.PI / 4;
	
	// neutral sound
	var qNeutral = 0;
	var isNeutral = r <= _game.settingsMgr.playerMaxNeutralRadius;
	var isNeutralFalloff = r <= _game.settingsMgr.playerNeutralRadiusFalloff;
	if (isNeutral) {
		// inside the neutral radius.
		qNeutral = 1;
	} 
	else if (isNeutralFalloff)
	{
		// in the neutral falloff radius.
		qNeutral = 1 - this.clamp(r, _game.settingsMgr.playerMaxNeutralRadius, _game.settingsMgr.playerNeutralRadiusFalloff) / _game.settingsMgr.playerNeutralRadiusFalloff;
	} 
	else {
		qNeutral = 0;
	}
	//console.log('neutral music: ' + qNeutral);
	this.getLayer('neutral').setVolume(qNeutral);
	
	if (!isNeutral) {
		// quad 1: hindi, boudhism
		var qu1diag = pi4;
		var qu1match = this.getMatch(qu1diag, r, t);
		//console.log('bhuddist music: ' + qu1match);
		this.getLayer('bhuddist').setVolume(qu1match);

		// quad 2: islam
		var qu2diag = 3 * pi4;
		//var qu2match = 1 - (Math.abs(qu2diag - t) / pi2);
		var qu2match = this.getMatch(qu2diag, r, t);
		//console.log('islamic music: ' + qu2match);
		this.getLayer('islamic').setVolume(qu2match);

		// quad 3: voodoo
		var qu3diag = 5 * pi4;
		var qu3match = this.getMatch(qu3diag, r, t);
		//console.log('voodoo music: ' + qu3match);
		this.getLayer('voodoo').setVolume(qu3match);

		// quad 4: wall street
		var qu4diag = 7 * pi4;
		var qu4match = this.getMatch(qu4diag, r, t);
		//console.log('wallstreet music: ' + qu4match);
		this.getLayer('wallstreet').setVolume(qu4match);
		this.getLayer('scientist').setVolume(qu4match);
	}
}

AudioManager.prototype.getMatch = function(targetAngle, r, t)
{
	var rd = 2.0 * Math.PI + targetAngle - t;
	//var diff = rd % Math.PI
	var diff = Math.atan2(Math.sin(rd), Math.cos(rd));
	
	var rawMatch =  1 - this.clamp(Math.abs(diff) / (Math.PI * 0.5), 0.0, 1.0);
	
	var maxRadius = game.settingsMgr.playerMaxRadius;
	var radiusFactor = this.clamp(r, 0, maxRadius) / maxRadius;
	
	return radiusFactor * rawMatch;
}

AudioManager.prototype.clamp = function(n, min, max)
{
	return Math.min(Math.max(n, min), max);
}

AudioManager.prototype.getLayer = function(layerName)
{
	var layer = null;
	for (var i in this.layers)
	{
		if (this.layers[i].name == layerName)
		{
			layer = this.layers[i];
			break;
		}
	}

	return layer;
}

/*AudioManager.prototype.makeDominant = function(layerName)
{
	for (var i in this.layers)
	{
		if (this.layers[i].name == layerName)
		{
			this.layers[i].setVolume(1);
		}
		else 
		{
			this.layers[i].setVolume(0.2);
		}
	}
}*/

AudioManager.prototype.addLayer = function(layerName)
{
	// Creates a new layer desc.
	var layer = new AudioLayer(layerName, this);
	
	// Loads the sound async.
	this.loadSoundAsync(layer, 'data/wysiwylf_' + layerName + '.wav');
	
	// Adds the layer.
	this.layers.push(layer);
	
	return layer;
}

AudioManager.prototype.loadSoundAsync = function(layer, filename)
{
	var that = this;
	var request = new XMLHttpRequest();
	request.open('GET', filename, true);
	request.responseType = 'arraybuffer';

	// Decode asynchronously
	request.onload = function() {
		that.context.decodeAudioData(
			request.response, 
			function(buffer) {
				layer.onBufferLoaded(buffer);
			},
			function() {
				console.log("Error loading sound " + filename);
			});
	}
	request.send();
}