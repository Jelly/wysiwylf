function lerp(a, b, t)
{
	return (b-a)*t+a;
}

function GameplayManager()
{
	this.direction = [0, 0];
	
	this.lastTileY = 0;
	this.sumFallingTiles = 0;
	this.createGround = false;
	this.groundCreatedCount = 0;
	
	this.lastPlayerTileY = 0;
	
	this.difficulties = new Array();
	this.difficulties['runner'] = 0;
	this.difficulties['trampoline'] = 0;
}

GameplayManager.prototype.update = function(_game)
{
	var playerTileY = Math.floor(game.playerMgr.position[1] / game.settingsMgr.tileSize[1]);
	if(playerTileY > this.lastPlayerTileY)
	{
		this.createGround = true;
	}

	if(this.lastPlayerTileY != playerTileY)
	{
		this.lastPlayerTileY = playerTileY;
	}
}

GameplayManager.prototype.createInitialSituation = function()
{
	// Create the first platform
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[1][1], [20, 40], 0)
	
	// Populate with some platforms
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[1][1], [-200, 100], 0)
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[1][1], [400, 200], 0)
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[1][1], [900, 100], 0)
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[2][1], [1500, 100], 0)
	game.tileMgr.addPlatformToTile(game.tileMgr.tiles[2][1], [2000, 200], 0)
	
	// ... and some trampolines
	game.tileMgr.addTrampolineToTile(game.tileMgr.tiles[1][1], [150, -250], 0)
	game.tileMgr.addTrampolineToTile(game.tileMgr.tiles[1][1], [300, -600], 0)
	
	// ... a ground too ?
	game.tileMgr.addGroundToTile(game.tileMgr.tiles[0][1], 'down', 0.8)
	game.tileMgr.addGroundToTile(game.tileMgr.tiles[1][1], 'down', 0.8)
	game.tileMgr.addGroundToTile(game.tileMgr.tiles[2][1], 'down', 0.8)
}

GameplayManager.prototype.setupTile = function(_tile)
{
	if(!game.tileMgr)
		return;
	
	var size = [ game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1] ];
	var posTile = [ _tile.index[0], _tile.index[1] ];
	var pos = [ posTile[0] * size[0], posTile[1] * size[1] ];

	// Update the player's general direction
	this.direction[0] += posTile[0];
	this.direction[1] += posTile[1];
	var norm = 1.0 / Math.sqrt(this.direction[0]*this.direction[0] + this.direction[1]*this.direction[1]);
	this.direction[0] *= norm;
	this.direction[1] *= norm;
	
	// Create a ground if needed	
	if(this.createGround)
	{
		if(pos[1] > game.playerMgr.position[1] - size[1] * 10)//== game.tileMgr.tiles[0][1] || _tile == game.tileMgr.tiles[1][1] || _tile == game.tileMgr.tiles[2][1])
		{
			game.tileMgr.addGroundToTile(_tile, 'down', 0.2)			
			
			this.groundCreatedCount++;
			
			if(this.groundCreatedCount == 3)
			{
				this.createGround = false;
				this.groundCreatedCount = 0;
			}
		}
	}
	
	// Generate runner gameplay ?
	if(Math.abs(this.direction[0]) > 0.5)
	{
		this.difficulties['runner']++;
		
		var rank = Math.min(3, Math.floor(this.difficulties['runner'] / 4));
		var coinRank = Math.min(2, rank);
		
		var runner = game.settingsMgr.runner;
	
		var startRand = [ Math.random() * runner.startRange[0] + runner.startOffset[0], Math.random() * runner.startRange[1] + runner.startOffset[1] ];
		var endRand = [ Math.random() * runner.endRange[0] + runner.endOffset[0], Math.random() * runner.endRange[1] + runner.endOffset[1] ];
	
		var start = [ pos[0] + size[0] * startRand[0], pos[1] + size[1] * startRand[1] ];
		var end = [ pos[0] + size[0] * endRand[0], pos[1] + size[1] * endRand[1] ]

		var count = 0;
		while(count < runner.maxCount)
		{
			var mix = count / (runner.maxCount - 1);
			
			var noise = (Math.random() - 0.5) * runner.noise * size[1];
			var p = [lerp(start[0], end[0], mix), lerp(start[1], end[1], mix) + noise];

			game.tileMgr.addPlatformToTile(_tile, p, rank);
			++count;
			
			// Randomly spawn a coin
			if(Math.random() > 0.25)
			{
				game.tileMgr.addCoinToTile(_tile, [p[0], p[1] - 50], coinRank);
			}
		}
	}
	else
	{
		this.difficulties['runner'] = 0;
	}
	
	// Generate trampoline gameplay ?
	if(this.direction[1] < -0.5)
	{
		this.difficulties['trampoline']++;
		
		var rank = Math.min(3, Math.floor(this.difficulties['trampoline'] / 4));
		var coinRank = Math.min(2, rank);
	
		var trampo = game.settingsMgr.trampoline;
		
		var startRand = [ Math.random() * trampo.startRange[0] + trampo.startOffset[0], Math.random() * trampo.startRange[1] + trampo.startOffset[1] ];
		var endRand = [ Math.random() * trampo.endRange[0] + trampo.endOffset[0], Math.random() * trampo.endRange[1] + trampo.endOffset[1] ];
	
		var start = [ pos[0] + size[0] * startRand[0], pos[1] + size[1] * startRand[1] ];
		var end = [ pos[0] + size[0] * endRand[0], pos[1] + size[1] * endRand[1] ]
		
		var count = 0;
		var maxCount = 3;
		while(count < trampo.maxCount)
		{
			var mix = count / (trampo.maxCount - 1);
			
			var p = [lerp(start[0], end[0], mix), lerp(start[1], end[1], mix)];
			
			game.tileMgr.addTrampolineToTile(_tile, p, rank);
			++count;
			
			// Randomly spawn a coin
			if(Math.random() > 0.25)
			{
				game.tileMgr.addCoinToTile(_tile, [p[0], p[1] - 50], coinRank);
			}
		}
	}
	else
	{
		this.difficulties['trampoline'] = 0;
	}
}
