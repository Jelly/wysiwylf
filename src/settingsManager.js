function SettingsManager()
{
	this.fps = 30;
	this.screenSize = [1280, 720];
	this.tileSize = this.screenSize;
	
	this.playerAcceleration = 100;
	this.playerSpeed = 100;
	this.playerJumpSpeed = 400;
	this.playerDiggingSpeed = 75;
	this.gravity = 300;
	
	this.trampolineRadius = 50;
	this.trampolineSpeed = 450;
	
	this.disableGravity = false;
	this.disableBackground = false;
	
	this.playerMaxRadius = 15000; // for audio engine
	this.playerMaxNeutralRadius = 300;
	this.playerNeutralRadiusFalloff = 1500;
	
	this.debugMode = false;
	this.disableMusic = false;
	
	// Runner gamemplay
	this.runner = 
	{
		maxCount: 3,
		startOffset: [0, 0.5],
		startRange: [0, 0],
		endOffset: [0.8, 0.5],
		endRange: [0, 0],
		noise: 0.3
	};
	this.trampoline = 
	{
		maxCount: 3,
		startOffset: [0.5, 0.1],
		startRange: [0, 0],
		endOffset: [0.5, 0.9],
		endRange: [0, 0]
	};
}
