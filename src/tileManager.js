function Tile(_x, _y)
{
	this.index = [_x, _y];
	this.position = [0, 0];

	// gameplay elements
	this.platforms = new Array();
	this.coins = new Array();
	this.trampolines = new Array();
	this.grounds = new Array();
	
	this.rect = new Sprite(game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	var surface = new Surface(game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	surface.context.strokeStyle = "rgb(64, 64, 64)"
	surface.context.strokeRect(0, 0, game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	this.rect.image = surface;
	this.rect.x = _x * game.settingsMgr.tileSize[0]
	this.rect.y = _y * game.settingsMgr.tileSize[1]
	game.sceneMgr.addDebugChild(this.rect);
}

Tile.prototype.remove = function()
{
	game.sceneMgr.removeDebugChild(this.rect);
	
	for(var i in this.platforms)
	{
		game.worldMgr.removePlatform(this.platforms[i]);
		this.platforms[i].remove();
	}
	for(var i in this.coins)
	{
		game.worldMgr.removePlatform(this.coins[i]);
		this.coins[i].remove();
	}
	for(var i in this.grounds)
	{
		game.worldMgr.removeGround(this.grounds[i]);
		this.grounds[i].remove();
	}
	for(var i in this.trampolines)
	{
		game.worldMgr.removeTrampoline(this.trampolines[i]);
		this.trampolines[i].remove();
	}
}

function TileManager()
{
	this.tileIndex = [0, 0]
	this.tiles = new Array();
	
	for(var y = 0; y <= 2; ++y)
	{
		this.tiles.push(new Array());
		for(var x = 0; x <= 2; ++x)
		{
			this.tiles[y].push(null);
		}
	}
	
	this.createColumn([-1, -1], 0);
	this.createColumn([-1, -1], 1);
	this.createColumn([-1, -1], 2);
	
	this.frame = 0;
}

TileManager.prototype.update = function(_game)
{
	if(this.frame == 1)
	{
		game.gameplayMgr.createInitialSituation();
	}
	this.frame++;

	var settings = _game.settingsMgr;
	this.tileIndex[0] = (_game.playerMgr.position[0] / settings.tileSize[0]);
	this.tileIndex[1] = (_game.playerMgr.position[1] / settings.tileSize[1]);
	
	this.borders = [
		Math.floor(this.tileIndex[0]-0.5), Math.floor(this.tileIndex[1]-0.5)
	];
	
	for(var x = 0; x <= 2; x += 2)
	{
		if(this.tiles[x][0])
		{
			if(x == 0 && this.tiles[x][0].index[0] > this.borders[0] )
			{
				this.removeColumn(2);
				this.moveColumn(1, 2);
				this.moveColumn(0, 1);
				this.createColumn(this.borders, 0);
			}
			else if(x == 2 && this.tiles[x][0].index[0] < this.borders[0] + 2 )
			{
				this.removeColumn(0);
				this.moveColumn(1, 0);
				this.moveColumn(2, 1);
				this.createColumn(this.borders, 2);
			}
		}
	}
	
	for(var y = 0; y <= 2; y += 2)
	{
		if(this.tiles[0][y])
		{
			if(y == 0 && this.tiles[y][0].index[1] > this.borders[1] )
			{
				this.removeLine(2);
				this.moveLine(1, 2);
				this.moveLine(0, 1);
				this.createLine(this.borders, 0);
			}
			else if(y == 2 && this.tiles[0][y].index[1] < this.borders[1] + 2 )
			{
				this.removeLine(0);
				this.moveLine(1, 0);
				this.moveLine(2, 1);
				this.createLine(this.borders, 2);
			}
		}
	}
}

TileManager.prototype.createColumn = function(_borders, _x)
{
	for(var i = 0; i <= 2; ++i)
	{
		var pos = [_borders[0] + _x, _borders[1] + i];
		var tile = this.createTile(pos[0], pos[1]);
		this.tiles[_x][i] = tile;
	}
}

TileManager.prototype.createLine = function(_borders, _y)
{
	for(var i = 0; i <= 2; ++i)
	{
		var pos = [_borders[0] + i, _borders[1] + _y];
		var tile = this.createTile(pos[0], pos[1]);
		this.tiles[i][_y] = tile;
	}
}

TileManager.prototype.removeColumn = function(_x)
{
	for(var i = 0; i <= 2; ++i)
	{
		this.tiles[_x][i].remove();
		this.tiles[_x][i] = null;
	}
}

TileManager.prototype.removeLine = function(_y)
{
	for(var i = 0; i <= 2; ++i)
	{
		this.tiles[i][_y].remove();
		this.tiles[i][_y] = null;
	}
}

TileManager.prototype.moveColumn = function(_from, _to)
{
	for(var i = 0; i <= 2; ++i)
	{
		this.tiles[_to][i] = this.tiles[_from][i];
		this.tiles[_from][i] = null;		
	}
}

TileManager.prototype.moveLine = function(_from, _to)
{
	for(var i = 0; i <= 2; ++i)
	{
		this.tiles[i][_to] = this.tiles[i][_from];
		this.tiles[i][_from] = null;		
	}
}

TileManager.prototype.createTile = function(_x, _y)
{
	var tile = new Tile(_x, _y);
	
	game.gameplayMgr.setupTile(tile);
	/*
	var isPlayerDigging = game.playerMgr.digging;
	
	// Add a ground if the player is falling too much.
	if (!isPlayerDigging && game.playerMgr.speed[1] > 2) {
		this.addGroundToTile(tile, "down");
	}
	
	// Debug: the 4 types of gameplay tiles.
	if (_x > 0 && _y == 0) {
		this.addRightRunnerToTile(tile);
	} else if (isPlayerDigging) {
		this.addDigToTile(tile);
	} else if (_x < 0 && _y == 0) {
		this.addLeftRunnerToTile(tile);
	} else if (_x == 0 && _y < 0) {
		this.addJumpToTile(tile);
	}
	*/
	return tile;
}

TileManager.prototype.addLeftRunnerToTile = function(tile) 
{
}

TileManager.prototype.addRightRunnerToTile = function(tile) 
{
	var maxX = game.settingsMgr.tileSize[0];
	var tileHeight = game.settingsMgr.tileSize[1];
	var maxMarginX = maxX / 6;
	var maxMarginY = maxX / 6; 
	
	var maxPlatformW = 300;
	
	var currentX = tile.index[0] * game.settingsMgr.tileSize[0];
	maxX += currentX
	
	while(currentX <= maxX) {
		// Random margin left
		currentX += Math.random() * maxMarginX;
		
		// Random height offset
		var marginBottom = Math.random() * maxMarginY;
		
		// Constrained platform width
		var distToMax = maxX - currentX;
		if (distToMax > 0) {
			// Make platform
			var pos = [currentX, tileHeight - marginBottom];
			//var size = [Math.min(maxPlatformW, distToMax), Math.random() * 34 + 30];
			var size = Math.floor(Math.random() * 2);
			this.addPlatformToTile(tile, pos, size);
			
			// Platform width + Random margin right
			currentX += size[0] + Math.random() * maxMarginX;
		}
	}
}

TileManager.prototype.addJumpToTile = function(tile) 
{
	var maxY = game.settingsMgr.tileSize[1];
	var tileWidth = game.settingsMgr.tileSize[0];
	var maxMarginX = maxY / 6;
	var maxMarginY = maxY / 6; 
	
	var maxPlatformW = 300;
	
	var currentY = tile.index[1] * game.settingsMgr.tileSize[1];
	maxY += currentY;
	
	while(currentY <= maxY) {
		// Random margin left
		currentY += Math.random() * maxMarginX;
		
		// Random height offset
		var marginLeft = Math.random() * tileWidth;
		
		// Constrained platform width
		var distToMax = maxY - currentY;
		if (distToMax > 0) {
			// Make platform
			var pos = [marginLeft, currentY];
			var size = Math.floor(Math.random() * 2);
			this.addTrampolineToTile(tile, pos, size);
			
			// Platform width + Random margin right
			currentY += size[1] + Math.random() * maxMarginY;
		}
	}
}

TileManager.prototype.addDigToTile = function(tile) 
{
}

TileManager.prototype.addPlatformToTile = function(tile, pos, size)
{
	// creates the platform and adds it to the scene.
	var platform = game.worldMgr.createPlatform(pos, size);
	
	// registers the platform.
	tile.platforms.push(platform)
	
	return platform;
}

TileManager.prototype.addCoinToTile = function(tile, pos, rank)
{
	// creates the platform and adds it to the scene.
	var coin = game.worldMgr.createCoin(pos, rank);
	
	// registers the platform.
	tile.coins.push(coin)
	
	return coin;
}

TileManager.prototype.addGroundToTile = function(tile, posDesc, _thickness)
{
	var tileSize = game.settingsMgr.tileSize;
	var edgeMargin = [tileSize[0] / 4, _thickness * tileSize[1]];
	
	var pos = [0, 0];
	var len = [0, 0];
	
	// calcs len and pos.
	if (posDesc == "down") {
		pos[1] = tileSize[1] - edgeMargin[1];
		len[0] = tileSize[0];
		len[1] = edgeMargin[1];
	}
	
	// conforms position
	var tilePos = [tile.index[0] * tileSize[0], tile.index[1] * tileSize[1]];
	
	// sets pos and len.
	var ground = game.worldMgr.createGround(tilePos, pos, len);
	
	// adds the ground.
	tile.grounds.push(ground);
	
	return ground;
}

TileManager.prototype.addTrampolineToTile = function(tile, pos, size)
{
	// creates the platform and adds it to the scene.
	var trampoline = game.worldMgr.createTrampoline(pos, size);
	
	// registers the platform.
	tile.trampolines.push(trampoline)
	
	return trampoline;
}