function Platform(_pos, _size)
{
	var x, y, w, h;
	
	switch (_size) {
		case 0:
			x = 5;
			y = 6;
			w = 244;
			h = 44;
			break;
		case 1:
			x = 6;
			y = 5;
			w = 201;
			h = 40;
			break;
		case 2:
			x = 5;
			y = 5;
			w = 153;
			h = 41;
			break;
		case 3:
			x = 5;
			y = 5;
			w = 118;
			h = 37;
			break;
	}
	
	this.position = _pos;
	this.size = _size;
	
	this.sprite = new Sprite(256, 64);
    game.sceneMgr.addTileChild(this.sprite);
	
	this.sprite.image = game.enchant.assets["data/platforms.png"];
	this.sprite.frame = _size;
	
	this.sprite.x = this.position[0] - w / 2 - x;
	this.sprite.y = this.position[1] - h / 2 - y;
		
	/*
	this.label = new enchant.Label();
    game.sceneMgr.addDebugChild(this.label);
	this.label.x = this.sprite.x;
	this.label.y = this.sprite.y;
	this.label.text = this.sprite.x + ', ' + this.sprite.y;
	*/
	
	// Remi, look at what you force me to do
	this.boundingBox = new BoundingBox({
		x: this.sprite.x,
		y: this.sprite.y,
		width: w,
		height: h
	});
}

Platform.prototype.remove = function()
{
    //game.sceneMgr.removeDebugChild(this.label);
    game.sceneMgr.removeTileChild(this.sprite);
}

Platform.prototype.update = function()
{
	// TODO dig
}
