var SceneManager = enchant.Class.create({
	initialize: function(scene) {
		this._scene = scene
		
		this._cameraControlledStage = new Group()
		this._scene.addChild(this._cameraControlledStage)
		
		this._hudStage = new Group()
		this._scene.addChild(this._hudStage)
		
		this._backgroundStage = new Group()
		this._cameraControlledStage.addChild(this._backgroundStage)
		
		this._gameplayStage = new Group()
		this._cameraControlledStage.addChild(this._gameplayStage)
		
		this._debugStage = new Group()
		//this._cameraControlledStage.addChild(this._debugStage)
	},
	
	addBackgroundChild: function(node) { this._backgroundStage.addChild(node) },
	removeBackgroundChild: function(node) { this._backgroundStage.removeChild(node) },
	
	addTileChild: function(node) { this._gameplayStage.addChild(node) },
	removeTileChild: function(node) { this._gameplayStage.removeChild(node) },
	
	addHudChild: function(node) { this._hudStage.addChild(node)	},
	removeHudChild: function(node) { this._hudStage.removeChild(node) },
	
	addDebugChild: function(node) { this._debugStage.addChild(node) },
	removeDebugChild: function(node) { this._debugStage.removeChild(node) },
	
	setCameraPosition: function(x, y) {
		this._cameraControlledStage.x = game.settingsMgr.screenSize[0] * 0.5 - x
		this._cameraControlledStage.y = game.settingsMgr.screenSize[1] * 0.5 - y
		
		//this._cameraControlledStage.x *= 0.4
		//this._cameraControlledStage.y *= 0.4
		
		//this._cameraControlledStage.originX = -this._scene.x
		//this._cameraControlledStage.originY = -this._scene.y
		
		//this._cameraControlledStage.scaleX = 0.4
		//this._cameraControlledStage.scaleY = 0.4
	}
});