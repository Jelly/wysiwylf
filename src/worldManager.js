function WorldManager()
{
	this.worldSize = 10000;	// Let's assume some numbers :p
    this.backgroundSize = [2048, 2048];
	this.backgroundPosition = [-this.backgroundSize[0]/2, -this.backgroundSize[1]/2-15];
	
    this.background = new Sprite(this.backgroundSize[0], this.backgroundSize[1]);
    if(!game.settingsMgr.disableBackground)
		game.sceneMgr.addBackgroundChild(this.background);
    this.background.image = game.enchant.assets['data/background.jpg'];
	this.background.scale(2, 2)
    
	this.speed = [0, 0];
    
	this.platforms = new Array();
	this.coins = new Array();
	this.trampolines = new Array();
	this.grounds = new Array();
	
	// groundSurfacePool START
	this.groundSurfacePool = 
	{
		surfaces: new Array(),
		
		getSurface: function()
		{
			for(var i in this.surfaces)
				if(this.surfaces[i].used == false)
				{
					this.surfaces[i].used = true;
					return this.surfaces[i].surface;
				}
		},
		releaseSurface: function(surface)
		{
			for(var i in this.surfaces)
				if(this.surfaces[i].surface == surface)
				{
					this.surfaces[i].used = false;
					return;
				}
		}
	}
	for(var i = 0; i < 18; ++i)
	{
		var surface = { 
			surface: new enchant.Surface(game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]),
			used: false
		}
		this.groundSurfacePool.surfaces.push(surface);
	}
	// groundSurfacePool END
	
	this.lifeTime = (2 * 60); // 2 minutes in seconds
	
	this.startTime = new Date();
	this.time = 0; // The time in seconds
	
	this.timeLabel = new enchant.Label();
    game.sceneMgr.addHudChild(this.timeLabel);
	this.timeLabel.x = 10;
	this.timeLabel.y = 10;
	this.timeLabel.text = 'Time left: 0';
	this.timeLabel.font = "25px 'Arial'";
}

WorldManager.prototype.update = function(_game)
{
    var settings = _game.settingsMgr;
    var speed = settings.playerSpeed;
    var position = settings.position;
	
	// Update the background position (let's smooth out)
	var newBackgroundPosition = [
		game.playerMgr.position[0] * 0.95 - this.backgroundSize[0] * 0.5,
		game.playerMgr.position[1] * 0.9 - this.backgroundSize[1] * 0.5 - 30,
	];
	this.backgroundPosition[0] -= (this.backgroundPosition[0] - newBackgroundPosition[0]) * 0.1;
	this.backgroundPosition[1] -= (this.backgroundPosition[1] - newBackgroundPosition[1]) * 0.1;
	
    this.background.x = this.backgroundPosition[0];
	this.background.y = this.backgroundPosition[1];
	
	// Update the platforms
	for(var i in this.platforms)
	{
		this.platforms[i].update();
	}

	// Update the grounds
	for(var i in this.grounds)
	{
		this.grounds[i].update();
	}

	// Update the coins
	for(var i in this.coins)
		this.coins[i].update(_game);
		
	// Update the time
	this.time = this.lifeTime - (new Date() - this.startTime) / 1000;
	var minutes = Math.floor(this.time / 60);
	var seconds = Math.floor(this.time % 60);
	this.timeLabel.text = 'Time left: ' + minutes + ':' + (seconds<10?'0':'') + seconds;
}

WorldManager.prototype.createPlatform = function(_pos, _size)
{
	var platform = new Platform(_pos, _size);
	this.platforms.push(platform);
	return platform;
}

WorldManager.prototype.removePlatform = function(_platform)
{
	for(var i in this.platforms)
		if(this.platforms[i] == _platform) {
			this.platforms.splice(i, 1);
			return;
		}
}

WorldManager.prototype.createCoin = function(_pos, _rank)
{
	var coin = new Coin(_pos, _rank);
	this.coins.push(coin);
	return coin;
}

WorldManager.prototype.removeCoin = function(_coin)
{
	for(var i in this.coins)
		if(this.coins[i] == _coin) {
			this.coins.splice(i, 1);
			return;
		}
}

WorldManager.prototype.createGround = function(_tilePos, _pos, _len)
{
	var ground = new Ground(_tilePos, _pos, _len);
	this.grounds.push(ground);
	return ground;
}

WorldManager.prototype.removeGround = function(_ground)
{
	for(var i in this.grounds)
		if(this.grounds[i] == _ground)
			this.grounds.splice(i, 1);
}

WorldManager.prototype.createTrampoline = function(_pos, _size)
{
	var trampoline = new Trampoline(_pos, _size);
	this.trampolines.push(trampoline);
	return trampoline;
}

WorldManager.prototype.removeTrampoline = function(_trampoline)
{
	for(var i in this.trampolines)
		if(this.trampolines[i] == _trampoline)
			this.trampolines.splice(i, 1);
}
