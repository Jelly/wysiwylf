function IsDefined(_var)
{
	return typeof(_var) != 'undefined';
}

function Game()
{
	// Create the settings manager
	this.settingsMgr = new SettingsManager();

	// Create the enchant.js context
	enchant();
	this.enchant = new Core(this.settingsMgr.screenSize[0], this.settingsMgr.screenSize[1]);
	this.enchant.keybind(32, 'space');
	
	document.getElementById('enchant-stage').style.border = 'solid 1px black';
}

Game.prototype.init = function()
{
	// Create the scene manager and replaces the root scene.
	this.sceneMgr = new SceneManager(this.enchant.rootScene);
	
	// Create the player manager
	this.playerMgr = new PlayerManager(); 
	
	// Create the world manager
	this.worldMgr = new WorldManager();
    
	// Create the gameplay manager
	this.gameplayMgr = new GameplayManager();
	
	// Create the tile manager
	this.tileMgr = new TileManager();

	// Create the audio manager.
	this.audioMgr = new AudioManager();
	
	// Create the renderer
	this.renderer = new Renderer();
}

Game.prototype.run = function()
{
	game.enchant.addEventListener('enterframe', function()
	{
		if(game.playerMgr)
			game.playerMgr.update(game);
			
		if(game.worldMgr)
			game.worldMgr.update(game);
			
		if(game.gameplayMgr)
			game.gameplayMgr.update(game);
			
		if(game.tileMgr)
			game.tileMgr.update(game);
			
		if(game.audioMgr)
			game.audioMgr.update(game);

		if(game.renderer)
			game.renderer.render(game);
	});

	game.enchant.start();
}

window.onload = function()
{
	game = new Game();	

	game.enchant.fps = game.settingsMgr.fps;
	
	enchant.ENV.USE_WEBAUDIO = true;
	
	var assets = [
		'data/background.jpg',
		'data/boing0.wav',
		'data/boing1.wav',
		'data/boing2.wav',
		'data/coin.wav',
		'data/coins.png',
		'data/earth.png',
		'data/grass.png',
		'data/guybrush.png',
		'data/jump.wav',
		'data/land.wav',
		'data/platforms.png',
		'data/scratch.wav',
		'data/step0.wav',
		'data/step1.wav',
		'data/step2.wav',
		'data/trampolines.png'
	];
	
	var initialized = false;
    game.enchant.onload = function()
	{
		if (!initialized) {
			initialized = true;
			game.init();
		}
    };
	
	game.enchant.preload(assets);
	
	game.run();
};
