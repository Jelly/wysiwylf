function Ground(_tilePos, _pos, _len)
{
	this.position = _tilePos;
	this.startPosition = _pos;
	this.length = _len;
	this.endPosition = [this.startPosition[0] + this.length[0], this.startPosition[1] + this.length[1]];
	
	this.rebuild();
}

Ground.prototype.remove = function()
{
	if (this.bgSprite != null)
	{
		game.worldMgr.groundSurfacePool.releaseSurface(this.bgSprite.image)
		this.bgSprite.image = null;
		game.sceneMgr.removeTileChild(this.bgSprite);
		this.bgSprite = null;
	}
	
	if (this.sprite != null)
	{
		game.worldMgr.groundSurfacePool.releaseSurface(this.sprite.image)
		this.sprite.image = null;
		game.sceneMgr.removeTileChild(this.sprite);
		this.sprite = null;
	}
}

Ground.prototype.rebuild = function()
{
	this.remove();
	
	// Background
	this.bgSprite = new enchant.Sprite(game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	this.bgSprite.x = this.position[0];
	this.bgSprite.y = this.position[1];
	
	var surface = game.worldMgr.groundSurfacePool.getSurface();
	this.bgSprite.image = surface;
	
    surface.context.globalAlpha = 0.4;
	
	// Clear the surface
	surface.context.globalCompositeOperation="source-over";
	surface.context.clearRect(0, 0, game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	
	// Draw the earth insides
	var earthImg = game.enchant.assets["data/earth.png"];
	var earthX = 0;
	while(earthX < this.length[0])
	{
		surface.draw(earthImg, earthX, this.startPosition[1]);
		earthX += earthImg.width;
	}
	
	game.sceneMgr.addTileChild(this.bgSprite);
	
	// Foreground
	this.sprite = new enchant.Sprite(game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	this.sprite.x = this.position[0];
	this.sprite.y = this.position[1];
	
	var surface = game.worldMgr.groundSurfacePool.getSurface();
	this.sprite.image = surface;
	
	// Clear the surface
	surface.context.globalCompositeOperation="source-over";
	surface.context.clearRect(0, 0, game.settingsMgr.tileSize[0], game.settingsMgr.tileSize[1]);
	
	// Draw the earth insides
	var earthImg = game.enchant.assets["data/earth.png"];
	var earthX = 0;
	while(earthX < this.length[0])
	{
		surface.draw(earthImg, earthX, this.startPosition[1]);
		earthX += earthImg.width;
	}
	
	// Draw the grass
	var grassImg = game.enchant.assets["data/grass.png"];
	var grassX = 0;
	while(grassX < this.length[0])
	{
		surface.draw(grassImg, grassX, this.startPosition[1] - grassImg.height + 10);
		grassX += grassImg.width;
	}
	
	game.sceneMgr.addTileChild(this.sprite);
	
	this.boundingBox = new BoundingBox(this.sprite)
	vec2.copy(this.boundingBox.min, this.startPosition)
	vec2.copy(this.boundingBox.max, this.endPosition)
	vec2.add(this.boundingBox.min, this.boundingBox.min, this.position)
	vec2.add(this.boundingBox.max, this.boundingBox.max, this.position)
}

Ground.prototype.update = function()
{
	var radius = 35;
	
	if (!game.playerMgr.digging)
		return;
		
	var playerPos = game.playerMgr.position;
	var tileSize = game.settingsMgr.tileSize;
	
	if( 	playerPos[0] < this.position[0] - radius || playerPos[0] > this.position[0] + tileSize[0] + radius
		||	playerPos[1] < this.position[1] - radius || playerPos[1] > this.position[1] + tileSize[1] + radius)
		return;
	
	var holePos = [ playerPos[0] - this.position[0], playerPos[1] - this.position[1] - radius ];

	var surface = this.sprite.image;	
	surface.context.fillStyle = "rgba(255, 255, 255, 0.2)";
	surface.context.globalCompositeOperation="destination-out";
	surface.context.beginPath();
	surface.context.arc(holePos[0], holePos[1], radius, 0, Math.PI*2, true);
	surface.context.fill();

}
