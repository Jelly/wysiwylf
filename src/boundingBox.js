function BoundingBox(sprite)
{
	this.min = vec2.clone([sprite.x, sprite.y])
	this.max = vec2.clone([sprite.x + sprite.width, sprite.y + sprite.height])
}

BoundingBox.prototype.collide = function(box)
{
	var collisionInfo = {
		normal: null,
		depth: null
	}
	
	function testOverlap(overlap, normal)
	{
		if (overlap < 0)
			return false
		
		if ((collisionInfo.depth === null) || (overlap < collisionInfo.depth))
		{
			// new minimal overlap found
			collisionInfo.depth = overlap
			collisionInfo.normal = normal
		}
		
		return true
	}
	
	if (!testOverlap(this.max[0] - box.min[0], [-1, 0])) return null
	if (!testOverlap(box.max[0] - this.min[0], [1, 0])) return null
	if (!testOverlap(this.max[1] - box.min[1], [0, -1])) return null
	if (!testOverlap(box.max[1] - this.min[1], [0, 1])) return null
	
	return collisionInfo
}
