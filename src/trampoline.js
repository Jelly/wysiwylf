function Trampoline(_pos, _size)
{
	var x, y, w, h;
	
	switch (_size) {
		case 0:
			x = 11;
			y = 10;
			w = 235;
			h = 44;
			break;
		case 1:
			x = 11;
			y = 8;
			w = 188;
			h = 44;
			break;
		case 2:
			x = 11;
			y = 11;
			w = 139;
			h = 44;
			break;
		case 3:
			x = 11;
			y = 10;
			w = 92;
			h = 44;
			break;
	}
	
	this.position = _pos;
	this.size = _size;
	
	this.sprite = new Sprite(256, 64);
    game.sceneMgr.addTileChild(this.sprite);
	
	this.sprite.image = game.enchant.assets["data/trampolines.png"];
	this.sprite.frame = _size;
	
	//this.label = new enchant.Label();
    //game.sceneMgr.addDebugChild(this.label);

	this.sprite.x = this.position[0] - w / 2 - x;
	this.sprite.y = this.position[1] - h / 2 - y;
		
	//this.label.x = this.sprite.x;
	//this.label.y = this.sprite.y;
	//this.label.text = this.sprite.x + ', ' + this.sprite.y;
	
	// Remi, look at what you force me to do
	this.boundingBox = new BoundingBox({
		x: this.sprite.x,
		y: this.sprite.y,
		width: w,
		height: h
	});
}

Trampoline.prototype.remove = function()
{
    //game.sceneMgr.removeDebugChild(this.label);
    game.sceneMgr.removeTileChild(this.sprite);
}