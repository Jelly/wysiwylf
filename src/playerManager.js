var frameOffsets = [
	[58, 114],
	[57, 114],
	[53, 114],
	null,
	[46, 116],
	[53, 112],
	[55, 114],
	[60, 114],
	[65, 112],
	[50, 112],
	[59, 123],
	[69, 111],
	[56, 117],
	[57, 115],
	[55, 112],
	[62, 113],
	[65, 91],
	[61, 96],
	[52, 93],
	[56, 93]
];

var animations = {
	start: {
		frames: [11],
		animationRate: 1
	},
	idle: {
		frames: [0, 1, 2, 1],
		animationRate: 3
	},
	walk: {
		frames: [4, 5, 6, 7, 8, 9],
		animationRate: 8
	},
	jump: {
		frames: [12, 13],
		animationRate: 4
	},
	land: {
		frames: [14, 15],
		animationRate: 4
	},
	digDown: {
		frames: [16, 17],
		animationRate: 4
	},
	digDownIdle: {
		frames: [17],
		animationRate: 1
	},
	dig: {
		frames: [18, 19],
		animationRate: 4
	},
	digIdle: {
		frames: [19],
		animationRate: 1
	}
}

function PlayerManager()
{
	this.playerSprite = new Sprite(128, 128);
	game.sceneMgr.addTileChild(this.playerSprite);
	
	this.playerSprite.image = game.enchant.assets["data/guybrush.png"];
	
	this.position = [0, 15];
	this.speed = [0, 0];
	
	this.grounded = false;
	this.digging = false;
	
	this.score = 0;
	
	this.setAnimation('start');
	
	this.boundingBox = new BoundingBox(this.playerSprite) // coordinates are wrong there; they will be overriden in update function
}

PlayerManager.prototype.setAnimation = function(name, transitionName)
{
	if (this.animation != name) {
		this.animation = name;
		this.frameIndex = -1;
		this.frameTimer = 0;
		
		var infos = transitionName ? animations[transitionName] : animations[name];
		this.frames = infos.frames;
		this.animationRate = infos.animationRate;
	}
}

PlayerManager.prototype.update = function(_game)
{
	var settings = _game.settingsMgr;
	var speed = settings.playerSpeed;
	var acc = settings.playerAcceleration;
	var fps = settings.fps;
	var digSpeed = settings.playerDiggingSpeed;
	
	var noMove = false;
	
	// Handle user inputs
	if(_game.enchant.input.right) {
		if (this.digging) {
			this.position[0] += digSpeed / fps;
		} else {
			if(this.speed[0] < -speed)
				this.speed[0] /= 1.2;
			else
			{
				if(Math.abs(this.speed[0]) > speed)
					this.speed[0] += acc / fps;
				else 
					this.speed[0] += speed;
			}
		}
		
		if (this.animation == 'start') {
			this.setAnimation('walk'/*, 'startToRight'*/);
		} else if (this.playerSprite.scaleX < 0) {
			this.playerSprite.scaleX = 1;
			if (this.digging)
				this.setAnimation('dig'/*, 'leftToRight'*/);
			else if (this.grounded)
				this.setAnimation('walk'/*, 'leftToRight'*/);
		} else if (this.digging)
			this.setAnimation('dig');
		else if (this.grounded)
			this.setAnimation('walk');
	}
	else if(_game.enchant.input.left) {
		if (this.digging) {
			this.position[0] -= digSpeed / fps;
		} else {
			if(this.speed[0] > speed)
				this.speed[0] /= 1.2;
			else
			{
				if(Math.abs(this.speed[0]) > speed)
					this.speed[0] -= acc / fps;
				else 
					this.speed[0] -= speed;
			}
		}
		if (this.animation == 'start') {
			this.playerSprite.scaleX = -1;
			this.setAnimation('walk'/*, 'startToLeft'*/);
		} else if (this.playerSprite.scaleX > 0) {
			this.playerSprite.scaleX = -1;
			if (this.digging)
				this.setAnimation('dig'/*, 'rightToLeft'*/);
			else if (this.grounded)
				this.setAnimation('walk'/*, 'rightToLeft'*/);
		} else if (this.digging)
			this.setAnimation('dig');
		else if (this.grounded)
			this.setAnimation('walk');
	}
	else {
		if (this.grounded && !this.digging) {
			this.speed[0] *= 0.8;
		}
		
		if(_game.enchant.input.down) {
			if (this.grounded) {
				this.digging = true;
			}
			if (this.digging) {
				this.position[1] += speed / fps;
				this.setAnimation('digDown');
			}
		}
		else {
			noMove = true;
		}
	}
		
	if (this.digging) {
		this.speed[0] = 0;
		this.speed[1] = 0;
	}
	
	if (_game.enchant.input.up)
	{
		if (this.digging) {
			this.position[1] -= speed / fps;
			this.setAnimation('digDown');
			noMove = false;
		} else if (this.grounded) {
			this.speed[1] -= settings.playerJumpSpeed;
			game.enchant.assets['data/jump.wav'].play();
		}
	}
	
	if (noMove || !this.digging)
		game.enchant.assets['data/scratch.wav'].stop();
	else if (this.digging)
		game.enchant.assets['data/scratch.wav'].play();
	
	var wasGrounded = this.grounded;
	this.grounded = false;
	
	// Handle gravity
	if(!settings.disableGravity && !this.digging)
		this.speed[1] += settings.gravity / settings.fps;
	
	// Integrate the speed
	this.position[0] += this.speed[0] / fps;
	this.position[1] += this.speed[1] / fps;
	
	// update bounding box before checking for collisions with environment
	this.boundingBox.min[0] = this.position[0] - 24
	this.boundingBox.max[0] = this.position[0] - 24 + 57
	this.boundingBox.min[1] = this.position[1] - 84
	this.boundingBox.max[1] = this.position[1] - 3
	
	// Handle collision with scenery
	if (this.speed[1] >= 0)
		for(var i in game.worldMgr.platforms)
		{
			this.collideWithBox(game.worldMgr.platforms[i].boundingBox, true)
		}
	
	for(var i in game.worldMgr.grounds)
	{
		this.collideWithBox(game.worldMgr.grounds[i].boundingBox)
	}
	
	// Handle collision with coins
	for(var i in game.worldMgr.coins)
	{
		this.collideWithCoin(game.worldMgr.coins[i]);
	}
	
	// Handle interaction with trampolines
	for(var i in game.worldMgr.trampolines)
	{
		var trampoline = game.worldMgr.trampolines[i]
		if (this.boundingBox.collide(trampoline.boundingBox))
		{
			this.speed[1] = - game.settingsMgr.trampolineSpeed;
			if (!this.boingTimer) {
				this.boingTimer = true;
				do {
					var boing = Math.floor(Math.random() * 3);
				} while (boing == this.lastBoing);
				this.lastBoing = boing;
				game.enchant.assets['data/boing' + boing + '.wav'].play();
				var that = this;
				setTimeout(function() {
					that.boingTimer = false;
				}, 300);
			}
		}
	}
	
	if (!this.grounded) {
		this.digging = false;
	} else if (!wasGrounded)
		game.enchant.assets['data/land.wav'].play();
	
	if (this.animation != 'start') {
		if (!this.grounded) {
			if (this.speed[1] < 0)
				this.setAnimation('jump');
			else
				this.setAnimation('land');
		} else if (noMove) {
			if (this.digging) {
				if (this.animation.indexOf('digDown') == 0)
					this.setAnimation('digDownIdle');
				else
					this.setAnimation('digIdle');
			} else
				this.setAnimation('idle');
		}
	}
	
	this.frameTimer -= 1 / settings.fps;
	while (this.frameTimer <= 0) {
		this.frameTimer += 1 / this.animationRate;
		++this.frameIndex;
		if (this.frameIndex >= this.frames.length) {
			this.frameIndex = 0;
			var infos = animations[this.animation];
			this.frames = infos.frames;
			this.animationRate = infos.animationRate;
		}
		
		if (this.animation == 'walk' && this.frameIndex % 3 == 0) {
			do {
				var step = Math.floor(Math.random() * 3);
			} while (step == this.lastStep);
			this.lastStep = step;
			game.enchant.assets['data/step' + step + '.wav'].play();
		}
	}
	
	var frame = this.frames[this.frameIndex];
	this.playerSprite.frame = frame;
	
	// Apply new position	
	if (this.playerSprite.scaleX > 0)
		this.playerSprite.x = this.position[0] - frameOffsets[frame][0] * this.playerSprite.scaleX;
	else
		this.playerSprite.x = this.position[0] -128 - frameOffsets[frame][0] * this.playerSprite.scaleX;
	this.playerSprite.y = this.position[1] - frameOffsets[frame][1];
	
	game.sceneMgr.setCameraPosition(this.position[0], this.position[1])
}

PlayerManager.prototype.collideWithBox = function(box, sideCollide)
{
	var collisionInfo = this.boundingBox.collide(box)
	
	if (!collisionInfo)
		return false
	
	// exclude this contact if it is separating
	if (vec2.dot(this.speed, collisionInfo.normal) > 0)
		return false
	
	if (!this.digging) {
		this.position[0] += collisionInfo.normal[0] * collisionInfo.depth
		this.position[1] += collisionInfo.normal[1] * collisionInfo.depth
	}
	
	// keep only tangent velocity
	var normalVelocity = [0, 0]
	vec2.scale(normalVelocity, collisionInfo.normal, vec2.dot(this.speed, collisionInfo.normal))
	vec2.subtract(this.speed, this.speed, normalVelocity)
	
	// check if we collided with the ground
	if (-collisionInfo.normal[1] > Math.abs(collisionInfo.normal[0]))
		this.grounded = true
	else if (Math.abs(collisionInfo.normal[0]) > Math.abs(collisionInfo.normal[1]) && !sideCollide) {
		if (!this.digging && this.position[1] - this.boundingBox.min < 1) { // seamless transition
			this.position[1] = this.boundingBox.min;
		} else {
			this.digging = true;
			this.grounded = true;
		}
	}
	
	return true;
}

PlayerManager.prototype.collideWithCoin = function(_coin)
{
	var dx = _coin.position[0] - this.position[0];
	var dy = _coin.position[1] - this.position[1] + 20;
	
	if (dx * dx + dy * dy < 2000) {
		this.score += _coin.takeAndGetPoints();
	}
}

PlayerManager.prototype.getPositionPolar = function()
{
	var x = this.position[0];
	var y = this.position[1];
	
	var r = Math.sqrt(x * x + y * y);
	var th = Math.atan2(y, x) + Math.PI;
	
	return [r, th];
}
