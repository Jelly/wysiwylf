// rank: 0 copper, 1 silver, 2 gold
function Coin(pos, rank)
{
	this.position = pos;
	this.rank = rank;
	
	this.sprite = new Sprite(64, 64);
    game.sceneMgr.addTileChild(this.sprite);
	
	this.sprite.image = game.enchant.assets["data/coins.png"];
	
	this.sprite.x = this.position[0] - 32;
	this.sprite.y = this.position[1] - 32;
	
	this.frameTimer = 0;
	this.frameIndex = -1;
}

Coin.animationRate = 6;

Coin.prototype.remove = function()
{
    game.sceneMgr.removeTileChild(this.sprite);
}

Coin.prototype.update = function(_game)
{
	var settings = _game.settingsMgr;
	
	this.frameTimer -= 1 / settings.fps;
	while (this.frameTimer <= 0) {
		this.frameTimer += 1 / Coin.animationRate;
		++this.frameIndex;
		if (this.frameIndex >= 4) {
			this.frameIndex = 0;
		}
	}
	
	var frame = 4 * this.rank + this.frameIndex;
	this.sprite.frame = frame;
}

Coin.prototype.takeAndGetPoints = function()
{
	if (this.sprite.visible) {
		game.enchant.assets['data/coin.wav'].play();
		this.sprite.visible = false;
		
		switch (this.rank) {
			case 0:
				return 1;
			case 1:
				return 2;
			case 2:
				return 5;
		}
	}
	return 0;
}
